/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.ktyp.lab2;

import java.util.Scanner;

/**
 *
 * @author toey
 */
public class Lab2 {

    Scanner sc = new Scanner(System.in);
    public String startgame;
    public boolean start;
    public String turn;
    public String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    public int row;
    public int column;
    private boolean end = false;

    public void inputStartgame() {
        System.out.println("----- Welcome to XO Game -----");
        System.out.print("Start XO Games? (Y/N) : ");
        startgame = sc.nextLine().toLowerCase();
        while (!startgame.equals("y") && !startgame.equals("n")) {
            System.out.print("Start XO Games? (Y/N) : ");
            startgame = sc.nextLine().toLowerCase();
        }
        if (startgame.equals("n")) {
            start = false;
        } else {
            start = true;
            printTable();
            inputFirstturn();
        }
    }

    public void printTable() {
        System.out.println(" ----------- ");
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print("  " + table[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(" ----------- ");

    }

    public void inputFirstturn() {
        System.out.print("Who first player? (X/O) : ");
        turn = sc.nextLine().toLowerCase();
        while (!turn.equals("x") && !turn.equals("o")) {
            System.out.print("Who first player? (X/O) :  ");
            turn = sc.nextLine().toLowerCase();
        }
        if (turn.equals("x")) {
            turn = "x";
        }
        if (turn.equals("o")) {
            turn = "o";
        } else {
            return;
        }
        System.out.println("--------------------------------");

    }

    public void printTurn() {
        System.out.println("Now " + turn.toUpperCase() + " turn");
    }

    public void inputRowColumn() {
        System.out.print("Please input row, column (1-3) : ");
        row = sc.nextInt();
        column = sc.nextInt();
        if (((row > 0 && row < 4) && (column > 0 && column < 4))) {
            if (table[row - 1][column - 1].equals("-")) {
                table[row - 1][column - 1] = turn.toUpperCase();

            } else {
                return;
            }
            printTurn();
            changeTurn();
        } else {
            System.out.println("**Plese input again** ");
            return;
        }
    }

    public void changeTurn() {
        if (turn.equals("x")) {
            turn = "o";
        } else {
            turn = "x";
        }
    }

    public void process() {
        printTurn();
        inputRowColumn();
    }

    public static void main(String[] args) {
        Lab2 project = new Lab2();
        project.inputStartgame();
        if (project.start == false) {
            System.out.println("----------- Goodbye -----------");
            project.end = true;
        }
        while (!project.end) {
            project.process();
        }
    }
}
